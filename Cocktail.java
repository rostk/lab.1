package com.company;

public class Cocktail {
    private String name;
    private double weight;
    private double price;
    private int numberOfIngredients;
    private int cookingTime;

    public static int totalPrice;

    public Cocktail(){

    }
    public Cocktail(String name, double weight, double price, int numberOfIngredients){

        setName (name);
        setWeight (weight);
        setPrice (price);
        setNumberOfIngredients (numberOfIngredients);

    }
    public Сocktail(String name, double weight, double price, int numberOfIngredients, int cookingTime){

        setName (name);
        setWeight (weight);
        setPrice (price);
        setNumberOfIngredients (numberOfIngredients);
        setCookingTime (cookingTime);
    }
    @Override
    public String toString() {
        return "Info about cocktail :  cocktail is called - " + name + ", its weight is - " + weight
                +"milliliters " + ", its price - " + price +"$" +", numberofingredients is - " + numberOfIngredients
                + ", cookingTime is - " + cookingTime+" min";
    }

    static void printStaticSum() {
        System.out.println("Total price is - " + totalPrice+" $");
    }

    public void printSum() {
        System.out.println("Cocktails with weight " + weight +"milliliters " +" is cooked " + cookingTime +" min"+ " and their price is " + totalPrice+" $");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumberOfIngredients() {
        return numberOfIngredients;
    }

    public void setNumberOfIngredients(int numberOfIngredients) {
        this.numberOfIngredients = numberOfIngredients;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public void resetValues(String name, double weight, double price, int numberOfIngredients, int cookingTime){

        setName (name);
        setWeight (weight);
        setPrice (price);
        setNumberOfIngredients (numberOfIngredients);
        setCookingTime (cookingTime);
    }

    public void setPrice(int price) {
        totalPrice -= this.price;
        this.price = price;
        totalPrice += this.price;
    }


}
