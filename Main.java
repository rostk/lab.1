package com.company;
import java.lang.String;
public class Main {

    public static void main(String[] args) {

        cocktail Standart = new Cocktail();
        cocktail Milky = new Cocktail("Milky", 100, 20, 4);
        cocktail Crimson = new Cocktail("Crimson", 70, 30, 6, 10);
        System.out.println(Standart.toString());
        System.out.println(Milky.toString());
        System.out.println(Crimson.toString());

        Standart.resetValues("chocolate", 60, 10, 4, 5);
        Milky.setPrice(30);


        Standart.printSum();
        Milky.printSum();
        Crimson.printSum();

        Cocktail.printStaticSum();




    }
}